from django.urls import path
from . import views

app_name = 'report'

urlpatterns = [
    path('', views.index, name='index'),
    path('new/', views.new, name='new'),
    path('save/', views.save, name='save'),
]