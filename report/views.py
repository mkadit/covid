from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from .forms import SuspectForm, SearchForm
from .models import Suspect

response = {}
# Create your views here.

def index(request):
    url_parameter = request.GET.get('q')

    if url_parameter:
        suspects = Suspect.objects.filter(nama_suspek__icontains=url_parameter)
        toggle = "collapse"
        if request.is_ajax():
            html = render_to_string(
                template_name="report/suspects.html", 
                context={"suspects": suspects}
            )

            data_dict = {"html_from_view": html}

            return JsonResponse(data=data_dict, safe=False)
    else:
        toggle = "collapse show"
        suspects = Suspect.objects.all()

    response['suspects'] = suspects
    response['toggle'] = toggle
    response['form'] = AuthenticationForm()

    return render(request, "report/index.html", response)

@login_required(login_url='/login/')
def new(request):
    response = {'inputForm': SuspectForm}
    return render(request, 'report/form.html', response)

def save(request):
    form = SuspectForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        data_form = form.cleaned_data
        data_input = Suspect()
        data_input.nama_suspek = data_form['nama_suspek']
        data_input.alamat = data_form['alamat']
        data_input.tanggal_lahir = data_form['tanggal_lahir']
        data_input.status = "Not Confirmed"
        data_input.save()
    return redirect('/report/')