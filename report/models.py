from django.db import models

# Create your models here.
class Suspect(models.Model):
    nama_suspek = models.CharField(max_length=30)
    alamat = models.CharField(max_length=200)
    tanggal_lahir = models.DateField()
    status = models.CharField(max_length=15)
