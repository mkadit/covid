from django.contrib.auth.models import User
from django.http import response
from django.test import TestCase, Client
from django.urls import reverse
from .models import Suspect
import datetime

# Create your tests here.
class testReport(TestCase):
    def setUp(self):
        Suspect.objects.create(nama_suspek="Udin", alamat="Jl. Aspal, Bumi", tanggal_lahir=datetime.datetime(2001, 9, 11))
        User.objects.create_user('temporary', password='temporary123')
        self.client.login(username='temporary', password='temporary123')

    def test_url_index(self):
        response = self.client.get('/report/')
        self.assertEqual(response.status_code, 200)

    def test_url_new(self):
        response = self.client.get(reverse('report:new'))
        self.assertEqual(response.status_code, 200)

    def test_template_index(self):
        response = self.client.get('/report/')
        self.assertTemplateUsed(response, 'report/index.html')

    def test_template_form(self):
        response = self.client.get(reverse('report:new'))
        self.assertTemplateUsed(response, 'report/form.html')

    def test_add_suspek_model(self):
        Suspect.objects.create(nama_suspek="Firaun", alamat="Jl. Pasir, Mesir", tanggal_lahir=datetime.datetime(1920, 7, 19))
        count = Suspect.objects.all().count()
        self.assertEqual(count, 2)

    def test_save_suspek_model(self):
        response = self.client.post('/report/save/', data = {"nama_suspek":"Kekeyi", "alamat":"Jl. Boneka, Rusia", "tanggal_lahir":"1995-3-23", "status":"Confirmed"})
        suspect = Suspect.objects.get(nama_suspek = 'Kekeyi')
        count = Suspect.objects.all().count()
        self.assertEqual(response.status_code, 302)
        self.assertEqual(count, 2)