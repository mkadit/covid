# Generated by Django 3.1.2 on 2020-11-16 12:10

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Suspect',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_suspek', models.CharField(max_length=30)),
                ('alamat', models.CharField(max_length=200)),
                ('tanggal_lahir', models.DateField()),
                ('status', models.CharField(max_length=15)),
            ],
        ),
    ]
