from django import forms


class Provinces(forms.Form):
    nama_provinsi = forms.CharField(label='Nama Provinsi Dicari', max_length=30,
                                    required=True, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nama Provinsi Dicari', 'id': 'search-url'}))


class createProvince(forms.Form):
    nama = forms.CharField(label='Nama Provinsi', max_length=30,
                           required=True, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nama Provinsi'}))

    kasus_positif = forms.IntegerField(label='Kasus Positif',
                                       required=True, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Kasus Positif'}))
    kasus_sembuh = forms.IntegerField(label='Kasus Sembuh',
                                      required=True, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Kasus Sembuh'}))
    kasus_meninggal = forms.IntegerField(label='Kasus Meninggal',
                                         required=True, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Kasus Meninggal'}))
