from django.urls import path
from . import views

app_name = 'tracker'

urlpatterns = [
    path('', views.index, name='index'),
    path('create_page', views.create_page, name='create_page'),
    path('search_page', views.search_page, name='search_page'),
    path('save_page', views.save_page, name='save_page')
]
