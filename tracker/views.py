from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.decorators import login_required
from . import forms, models
from django.http import JsonResponse
import urllib3
from json import loads

# Create your views here

def index(request):
    form = { 'search_form': forms.Provinces(), 'form':AuthenticationForm()}
    return render(request, 'tracker/index.html', form)

def search_page(request):
    http = urllib3.PoolManager()
    response = http.request(
        'GET', "https://services5.arcgis.com/VS6HdKS0VfIhv8Ct/arcgis/rest/services/COVID19_Indonesia_per_Provinsi/FeatureServer/0/query?where=1%3D1&outFields=*&outSR=4326&f=json")
    data = loads(response.data.decode('utf-8'))
    data_provinsi = models.Provinsi.objects.all()
    positif = 0
    sembuh = 0
    meninggal = 0
    for i in data['features']:
        positif += i['attributes']['Kasus_Posi']
        sembuh += i['attributes']['Kasus_Semb']
        meninggal += i['attributes']['Kasus_Meni']
    data['features'][34] = {'attributes': {'Provinsi': 'Indonesia', 'Kasus_Posi': positif,
                                                 'Kasus_Semb': sembuh, 'Kasus_Meni': meninggal}}
    for i in data_provinsi:
        p_data = {'attributes': {'Provinsi': i.nama, 'Kasus_Posi': i.kasus_positif,
                               'Kasus_Semb': i.kasus_sembuh, 'Kasus_Meni': i.kasus_meninggal}}
        data['features'].append(p_data)
    return JsonResponse(data)


@login_required(login_url='/login/')
def create_page(request):
    data = {
        'create_form': forms.createProvince()}
    return render(request, 'tracker/create.html', data)

def save_page(request):
    activity_form = forms.createProvince(request.POST or None)
    if request.method == "POST":
        if activity_form.is_valid():
            provinsi = models.Provinsi()
            provinsi.nama = activity_form.cleaned_data['nama']
            provinsi.kasus_positif = activity_form.cleaned_data['kasus_positif']
            provinsi.kasus_sembuh = activity_form.cleaned_data['kasus_sembuh']
            provinsi.kasus_meninggal = activity_form.cleaned_data['kasus_meninggal']
            provinsi.save()
        return redirect('/tracker/')
