# Kelompok C-01
## Anggota
1. Muhammad Falihadib 1906299042
2. Hasiana Emanuela Rajagukguk 1906293083
3. Siti Noer Cholizzhah Haeruddin 1906299162
4. Muhammad Krishertanto Adityaputro 1906302485

Link gitlab: https://gitlab.com/mkadit/covid

Link Heroku: http://living-through-covid.herokuapp.com/

## Cerita Aplikasi 

Hingga saat ini, wabah pandemi COVID-19 yang sudah berlangsung lebih dari tujuh
bulan belum juga berakhir. Masyarakat dari berbagai kalangan memiliki
keresahannya masing-masing untuk untuk menjalani hidup di kondisi seperti ini.
Selain resah dengan keadaan sekarang, masyarakat juga memiliki banyak kendala
mengenai dimana mereka harus mencari informasi yang tepat untuk menghindarkan
diri dari wabah ini. Kami sebagai mahasiswa Fasilkom ingin turut serta membantu
masyarakat dengan kemampuan kami dalam membuat website.  Dalam website ini,
kami ingin menyajikan informasi yang tepat dan akurat terkait dengan COVID-19
agar masyarakat tetap mengikuti perkembangan dari kondisi yang ada. 


## Feature

### Mini-Test Gejala COVID

Pada fitur ini, pengunjung dapat mengikut sebuah tes kecil yang menanyakan
seputar kesehatannya. Dari test ini, dapat diketahui kemungkinan-kemungkinan
penyakit yang dimiliki pengunjung, termasuk kemungkinan COVID.  Selain itu,
pengunjung dapat meng-submit saran mengenai suatu penyakit atau gejala kepada
admin, sehingga admin dapat mengupdate test tersebut.

### Report Penduduk COVID

Pada fitur ini, pengunjung dapat melaporkan apabila ada penduduk yang memiliki
gejala COVID melewati suatu form. Pemegang website dapat mengkonfirmasi apakah
penduduk ini benar-benar memiliki COVID atau tidak, dapat dilihat dari status
Confirmed/Not Confirmed. Akan ditampilkan siapa saja yang merupakan suspect dan
siapa saja yang sudah terkonfirmasi di halaman utama fitur ini, dan dapat
dilihat juga di daerah mana penduduk-penduduk itu berada.  

### Provinsi Tracker
Fitur ini berisi list provinsi yang menderita penyakit COVID-19. Dalam
database, terdapat provinsi, di mana ada informasi mengenai nama provinsi,
total kasus, jumlah penderita yang hidup, dan yang mati. Di dalam ffitur ini
anda juga dapat menambahkan atau melakukan search (case-insensitive) pada
provinsi yang ada.

### Anti-COVID Starter Kit
Pada fitur ini, terdapat list-list yang berisi hal-hal apa saja yang diperlukan
atau dilakukan pada masa pandemi, misalnya seperti list benda-benda yang perlu
dibawa saat bepergian. Terdapat banyak kategori list yang dapat dilihat dan
digunakan seperti checklist. Pengunjung juga dapat menambahkan list-list sesuai
kebutuhan mereka, dan list-list ini dapat digunakan oleh pengunjung lainnya.

