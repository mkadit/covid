var que = '';

$(document).ready(function () {
	$.ajax({
		url: window.location.origin + '/tracker/search_page',
		fail: function (data) {
			console.log(data);
		},
		success: function (data) {
			console.log(data);
			let que = $('#search-url').val();
			let parent = $('#tbody');
			let inner = '';
			let json = data;
			console.log(json.features[0].attributes.Provinsi);
			for (let i = 0; i < json.features.length; i++) {
				if (json.features[i].attributes.Provinsi.includes(que)) {
					inner += '<tr>';
					inner +=
						'<th>' + json.features[i].attributes.Provinsi + '</th>';
					inner +=
						'<th>' +
						json.features[i].attributes.Kasus_Posi +
						'</th>';
					inner +=
						'<th>' +
						json.features[i].attributes.Kasus_Semb +
						'</th>';
					inner +=
						'<th>' +
						json.features[i].attributes.Kasus_Meni +
						'</th>';
					inner += '</tr>';
				}
			}
			parent.html(inner);
		},
	});
});

$('#popup').click(function (e) {
	document.querySelector('.modal').classList.add('is-active');
});

$('.modal-background').click(function (e) {
	document.querySelector('.modal').classList.remove('is-active');
});

$('#search-form').submit(function name(e) {
	e.preventDefault();

	$.ajax({
		url: window.location.origin + '/tracker/search_page',
		fail: function (data) {
			console.log(data);
		},
		success: function (data) {
			console.log(data);
			let que = $('#search-url').val();
			let parent = $('#tbody');
			let inner = '';
			let json = data;
			console.log(json.features[0].attributes.Provinsi);
			for (let i = 0; i < json.features.length; i++) {
				if (json.features[i].attributes.Provinsi.includes(que)) {
					inner += '<tr>';
					inner +=
						'<th>' + json.features[i].attributes.Provinsi + '</th>';
					inner +=
						'<th>' +
						json.features[i].attributes.Kasus_Posi +
						'</th>';
					inner +=
						'<th>' +
						json.features[i].attributes.Kasus_Semb +
						'</th>';
					inner +=
						'<th>' +
						json.features[i].attributes.Kasus_Meni +
						'</th>';
					inner += '</tr>';
				}
			}
			parent.html(inner);
		},
	});
});
