from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from .forms import TambahPertanyaan
from .models import Pertanyaan

# Create your views here.
def index(request):
    return render(request, 'quiz/index.html', {'form': AuthenticationForm()})

@login_required(login_url='/login/')
def quiz(request):
    return render(request, 'quiz/quiz.html')

@login_required(login_url='/login/')
def result(request):
    counter = 0
    if request.method == 'POST':
        for i in range(10):
            value = request.POST.get('{}'.format(i + 1))
            if value == '1':
                counter += 1
            elif value == '2':
                counter += 2
            elif value == '3':
                counter += 3
            elif value == '5':
                counter += 5
            else:
                counter += 0
    else:
        return render(request, 'quiz/quiz.html')
    context = {
        'counter': counter
    }
    return render(request, 'quiz/result.html', context)

@login_required(login_url='/login/')
def suggest(request):
    if request.method == 'POST':
        form = TambahPertanyaan(request.POST)
        if form.is_valid():
            form.save()
            return redirect('suggested')
    else:
        form = TambahPertanyaan()
    return render(
        request,
        'quiz/suggest.html',
        {
            'form': form
        }
    )

@login_required(login_url='/login/')
def suggested(request):
    q = Pertanyaan.objects.all()
    return render(
        request, 
        'quiz/suggested.html',
        {
            'q': q
        }
    )

def data(request):
    data = list(Pertanyaan.objects.values())
    return JsonResponse(data, safe=False)