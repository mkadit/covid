from django.forms import ModelForm, TextInput
from .models import Pertanyaan

class TambahPertanyaan(ModelForm):
    class Meta:
        model = Pertanyaan
        fields = ['pertanyaan']
        labels = {'pertanyaan': False}
        widgets = {
            'pertanyaan': TextInput(attrs={'class': 'control input', 'placeholder':'Insert a question here..'}),
        }
