from django.urls import path
from . import views

urlpatterns = [
    path('data/', views.data, name='data'),
    path('suggested/', views.suggested, name='suggested'),
    path('suggest/', views.suggest, name='suggest'),
    path('result/', views.result, name='result'),
    path('start/', views.quiz, name='start'),
    path('', views.index, name='index')
]
