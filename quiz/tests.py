from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth.models import User

from .models import Pertanyaan
from .views import index, quiz, result

# Create your tests here.
class testQuiz(TestCase):
    def setUp(self):
        self.user = User.objects.create_user('liza', 'csitinoer@yahoo.com', 'hello123')

    def test_model_nama(self):
        Pertanyaan.objects.create(pertanyaan="muntah")
        stri = Pertanyaan.objects.get(pertanyaan="muntah").__str__()
        self.assertEqual(stri, "muntah")

    def test_template_index(self):
        response = Client().get('/quiz/')
        self.assertTemplateUsed(response, 'quiz/index.html')

    def test_template_quiz(self):
        self.client.login(username= 'liza', password= 'hello123')
        response = self.client.get(reverse('start'))
        self.assertTemplateUsed(response, 'quiz/quiz.html')

    def test_template_suggest(self):
        self.client.login(username= 'liza', password= 'hello123')
        response = self.client.get(reverse('suggest'))
        self.assertTemplateUsed(response, 'quiz/suggest.html')

    def test_template_suggested(self):
        self.client.login(username= 'liza', password= 'hello123')
        response = self.client.get(reverse('suggested'))
        self.assertTemplateUsed(response, 'quiz/suggested.html')

    def test_template_result(self):
        self.client.login(username= 'liza', password= 'hello123')
        response = self.client.post(reverse('result'))  
        self.assertTemplateUsed(response, 'quiz/result.html')
    
    def test_json(self):
        p = Pertanyaan(pertanyaan='yabegitu')
        p.save()
        data = list(Pertanyaan.objects.values())
        self.assertTrue(data[0]['pertanyaan'], 'yabegitu')

    def test_json(self):
        p = Pertanyaan(pertanyaan='yabegitu')
        p.save()
        data = list(Pertanyaan.objects.values())
        response = Client().get('/quiz/data')
        self.assertTrue(data[0]['pertanyaan'], 'yabegitu')
        self.assertTrue(response.status_code, 200)

    
    
    
    
    