from django.db import models

# Create your models here.
class Pertanyaan(models.Model):
    pertanyaan = models.CharField(max_length=500)

    def __str__(self):
        return "{}".format(self.pertanyaan)