from django.test import TestCase, Client
from django.http import response
import requests
from .models import things_list
from django.contrib.auth.models import User
from django.urls import reverse

class TestTodolist(TestCase):
    def setUp(self):
        self.credentials = { 'username': 'anjing', 'password': 'aanjayani123!'}
        User.objects.create_user('anjing', 'aku@guk.com', 'aanjayani123!')
        self.client.login(username='anjing', password='aanjayani123!')

    def test_url(self):
        response = self.client.get('/todolist/')
        self.assertEqual(response.status_code, 200)

    def test_html_template(self):
        response = self.client.get('/todolist/')
        self.assertTemplateUsed(response, 'todolist/index.html')

    def test_add_todolist(self):
        things_list.objects.create(
            nama_list="capek",
            deskripsi="capek",
            todo1="capek",
            todo2="capek",
            todo3="capek",
            todo4="capek",
            todo5="capek",
            tobring1="capek",
            tobring2="capek",
            tobring3="capek",
            tobring4="capek",
            tobring5="capek",
        )
        count = things_list.objects.all().count()
        self.assertEqual(count, 1)
    
    def test_make_new(self):
        response = self.client.get(reverse('todolist:form'))
        self.assertEqual(response.status_code, 200)

    def test_html_template_form(self):
        response = self.client.get(reverse('todolist:form'))
        self.assertTemplateUsed(response, 'todolist/form.html')

    def test_post_todolist(self):
        response = self.client.post(reverse('todolist:form'), {
                "nama_list": "kucinganjing",
                "deskripsi": "kucinganjing",
                "todo1": "kucinganjing",
                "todo2": "kucinganjing",
                "todo3": "kucinganjing",
                "todo4": "kucinganjing",
                "todo5": "kucinganjing",
                "tobring1": "kucinganjing",
                "tobring2": "kucinganjing",
                "tobring3": "kucinganjing",
                "tobring4": "kucinganjing",
                "tobring5": "kucinganjing",
            }
        )
        todolist = self.client.get(reverse('todolist:form'))
        count = things_list.objects.all().count()
        self.assertEqual(count, 1)
        self.assertEqual(response.status_code, 302)

    def test_get_todolist(self):
        response = self.client.post(reverse('todolist:form'), {
                "nama_list": "astagacapek",
                "deskripsi": "astagacapek",
                "todo1": "astagacapek",
                "todo2": "astagacapek",
                "todo3": "astagacapek",
                "todo4": "astagacapek",
                "todo5": "astagacapek",
                "tobring1": "astagacapek",
                "tobring2": "astagacapek",
                "tobring3": "astagacapek",
                "tobring4": "astagacapek",
                "tobring5": "astagacapek",
            }
        )
        count = things_list.objects.all().count()
        self.assertEqual(count, 1)
        self.assertEqual(response.status_code, 302)

        response2 = self.client.get(reverse("todolist:every_list", args=['1']))
        self.assertEqual(response2.status_code, 200)

        response2 = self.client.post(reverse("todolist:every_list", args=['1']))
        self.assertEqual(response2.status_code, 302)