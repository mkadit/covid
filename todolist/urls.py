from django.urls import path
from . import views

app_name = 'todolist'

urlpatterns = [
    path('', views.home, name='home'),
    path('form/', views.form, name='form'),
    path('<int:num>/', views.every_list, name='every_list'),
    path('search/<str:query>/', views.search_list, name='search'),
]