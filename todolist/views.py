from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.http import JsonResponse
from .models import things_list
from .forms import todo_list

def home(request):
    items = things_list.objects.all()
    context = {'lists' : items, 'form': AuthenticationForm()}
    return render(request, 'todolist/index.html', context)

@login_required(login_url='/login/')
def form(request):
    if request.method == "POST":
        form = todo_list(request.POST)
        if form.is_valid():
            form.save()
        return redirect('todolist:home')
    else:
        form = todo_list()
        return render(request, 'todolist/form.html', {'todo_list':form})

def every_list(request, num):
    items = things_list.objects.get(id=num)
    if request.method == 'POST':
        items.delete()
        return redirect('todolist:home')
    context = {'lists': items}
    return render(request, 'todolist/perlist.html', context)

def search_list(request, query):
    semua_list = things_list.objects.filter(nama_list__icontains = query)
    masuk_objek_list = {}
    items = []
    length = 0

    for i in range (len(semua_list)):
        length += 1
        list = semua_list[i]
        items.append({
            "id": list.id,
            "nama": list.nama_list,
            "deskripsi": list.deskripsi,
            "todo1": list.todo1,
            "todo2": list.todo2,
            "todo3": list.todo3,
            "todo4": list.todo4,
            "todo5": list.todo5,
            "tobring1": list.tobring1,
            "tobring2": list.tobring2,
            "tobring3": list.tobring3,
            "tobring4": list.tobring4,
            "tobring5": list.tobring5
        })

    masuk_objek_list["items"] = items
    masuk_objek_list["length"] = length
    return JsonResponse(masuk_objek_list)