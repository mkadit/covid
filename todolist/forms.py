from django import forms
from .models import things_list

class todo_list(forms.ModelForm):
    class Meta:
        model = things_list
        fields = ['nama_list', 'deskripsi', 'todo1', 'todo2','todo3','todo4','todo5', 'tobring1', 'tobring2', 'tobring3', 'tobring4', 'tobring5']